# Simple node command line app for adding notes to a JSON file #


## To add note to json file.... ##

<pre><code>node app.js add --title="xxx" --body="xxx"</code></pre>

## To remove note from json file.... ##

<pre><code>node app.js remove --title="xxx"</code></pre>

## To list notes ##

<pre><code>node app.js list --title="xxx"</code></pre>

## To read notes ##
<pre><code>node app.js read --title="xxx"</code></pre>