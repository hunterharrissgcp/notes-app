const fs = require('fs');
const chalk = require('chalk');

// List notes from notes.js
const listNotes = () => {
    console.log(chalk.inverse('Your notes'));
    const notes = loadNotes();

    notes.forEach(note => {
        console.log(note.title);
    }); 
}

// Add note to notes.json
const addNote = (title, body) => {
    const notes = loadNotes();
    const duplicate = notes.find(note => {
        return note.title === title
    });

    if(!duplicate) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes);
        console.log(chalk.green.inverse('Note added'))
    } else {
        console.log(chalk.red.inverse('Title Taken'));
    }
}

// Remove note from notes.js
const removeNote = (title) => {
    const notes = loadNotes();
    const keptNotes = notes.filter(note => {
        return note.title !== title
    });

    if(notes.length > keptNotes.length){
        console.log(chalk.green.inverse('Note removed'));
        saveNotes(keptNotes);
    } else {
        console.log(chalk.red.inverse('No Note found'));
    }
}

// Saves note to notes.json
const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes);
    fs.writeFileSync('notes.json', dataJSON);
}

// Loads notes from notes.json
const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json');
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    } catch(e) {
        return [];
    }
}

// Read a particular note
const readNote = (title) => {
    const notes = loadNotes();
    const note = notes.find(note => note.title === title);

    if(note){
        console.log(chalk.inverse(note.title));
        console.log(note.body);
    } else {
        console.log(chalk.red.inverse('Note note\ found'));
    }
}

module.exports =  {
    getNotes: listNotes,
    addNote: addNote,
    removeNote: removeNote,
    readNote: readNote
}