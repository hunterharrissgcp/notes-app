const chalk = require('chalk');
const yargs = require('yargs');
const notes = require('./notes');

// Adds a new note
yargs.command({
    command: 'add',
    describe: 'Adds a new note',
    builder: {
        title: {
            describe: 'Notes title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Notes body',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
      notes.addNote(argv.title, argv.body);
    }
})

// Removes a note
yargs.command({
    command: 'remove',
    describe: 'Removes a note',
    builder: {
        title: {
            describe: 'Notes title',
            demandOption: true,
            type: 'string'
        },
    },
    handler: (argv) => {
        notes.removeNote(argv.title);
    }
})

// Returns a list of notes
yargs.command({
    command: 'list',
    describe: 'Returns a list of notes',
    builder: {
        title: {
            describe: 'Notes title'
        }
    },
    handler: (argv) => {
        notes.getNotes();
    }
})

// Reads a note
yargs.command({
    command: 'read',
    describe: 'Reads a note',
    builder: {
        title: {
            describe: 'Notes title',
            demandOption: true,
            type: 'string'
        }
    },
    handler: (argv) => {
        notes.readNote(argv.title);
    }
});

yargs.argv;